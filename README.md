<br />
<p align="center">
  <div align="center"><h1>nestjs-microservice-test</h1></div>
</p>
</br>



## How to initiate

git clone this repo
just 'docker-compose up -d' and the server will startup :)

The user is created when the userService module is initialized with values
{username: 'john', password: 'testpassword'}

I left a Postman collection with its environment variables you can use to test the service.
Just import the collection and the ENV to your Postman, the JWT for the headers are automated.

## Notes

I know there are some things to improve in the code and I'm on it. I'd apreciate feedbacks :)

## Tools
- docker-compose version 1.29.2
- Docker version 20.10.10
- Node -v 14.18.1