import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDocument, User } from 'src/models/user.model';
import * as bcrypt from 'bcrypt';
@Injectable()
export class UsersService implements OnModuleInit{
	//Injects a dummy user
	async onModuleInit(){
		await this.create({username: 'john', password: 'testpassword'})
	}

	constructor(
		@InjectModel(User.name) private readonly users: Model<UserDocument>,
	) {}

	async findOne(username: string): Promise<User | undefined> {
		return this.users.findOne({username});
	}

	async create(user: User) {
		user.password = bcrypt.hashSync(user.password,10);
		return  this.users.findOneAndUpdate({username: user.username}, user, {
			new: true,
			upsert: true
		});
	}
}
