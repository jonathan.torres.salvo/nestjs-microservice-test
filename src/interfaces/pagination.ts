import { IsNumber, Min, IsOptional, Max, IsString, IsEnum } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
 
export enum FilterNew  { 'author' , '_tags' , 'title' , 'month_word' };

export class PaginationParams {
  @ApiProperty({
    required:false,
    description: "This value will be used to select the filter type",
    enum: FilterNew,
  })
  @IsOptional()
  @Type(() => String)
  @IsEnum(FilterNew)
  filter_by?: FilterNew;

  @ApiProperty({
    required:false,
    description: "This value will be used as value for the filter",
    type: String,
  })
  @IsOptional()
  @Type(() => String)
  @IsString()
  value?: string;
  
  @ApiProperty({
    required:false,
    description: "This value will be used as limit for pagination",
    type: Number,
    maximum: 5
  })
  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @Max(5)
  limit?: number;
}