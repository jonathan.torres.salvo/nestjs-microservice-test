import { ApiProperty } from "@nestjs/swagger";
export class User {
    @ApiProperty({
        example: 'john'
    })
	username: string;
    @ApiProperty({
        example: 'changeme'
    })
	password: string;
};