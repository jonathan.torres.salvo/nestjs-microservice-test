import { Controller, Delete, Get, Param, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiProperty, ApiTags } from '@nestjs/swagger';
import { PaginationParams } from '../interfaces/pagination';
import { New } from '../interfaces/new';
import { NewsService } from './news.service';

@Controller('news')
@ApiBearerAuth()
@ApiTags('News')
export class NewsController {
	constructor(private newsService: NewsService) {}
	@Get()
	@ApiOkResponse({
		description: 'The body of the requested news',
		type: New,
		isArray: true,
	})
	
	async all(@Query() { filter_by, value, limit }: PaginationParams) {
		return this.newsService.findAll(filter_by, value , limit);
	}

	@Get(':id')
	@ApiProperty({
		type: String,
		required: true,
		description: 'id to be searched'
	})
	@ApiOkResponse({
		description: 'The body of the requested new',
		type: New,
	})
	findOne(@Param('id') id: string) {
		return this.newsService.findOne(id);
	}
	@ApiProperty({
		type: String,
		required: true,
		description: 'id to be deleted'
	})
	@Delete(':id')
	deleteOne(@Param('id') id: string) {
		return this.newsService.delete(id);
	}


}
