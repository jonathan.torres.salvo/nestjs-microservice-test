import { Module } from '@nestjs/common';
import { NewsController } from './news.controller';
import { NewsCronService } from './news-cron.service';
import { MongooseModule } from '@nestjs/mongoose';
import { NewSchema } from '../models/new.model';
import { NewsService } from './news.service';
import { HttpModule } from '@nestjs/axios';
@Module({
	imports: [
		MongooseModule.forFeature([{ name: 'New', schema: NewSchema }]),
		HttpModule,
	],
	controllers: [NewsController],
	providers: [NewsCronService, NewsService],
})
export class NewsModule {}
