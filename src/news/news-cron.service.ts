import { HttpService } from '@nestjs/axios';
import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron, CronExpression } from '@nestjs/schedule';
import { lastValueFrom } from 'rxjs';
import { New } from 'src/models/new.model';
import { NewsService } from './news.service';

@Injectable()
export class NewsCronService implements OnModuleInit {
	private readonly logger = new Logger(NewsCronService.name);
	private newsUrl: string;

	async onModuleInit() {
		await this.handleCron();
	}

	constructor(
		private httpService: HttpService,
		private configService: ConfigService,
		private newService: NewsService,
	) {
		this.newsUrl = this.configService.get<string>('NEWS_URL');
	}
	
	@Cron(CronExpression.EVERY_HOUR)
	async handleCron() {
		try {
			this.logger.log('Updating news');
			const news = await this.getNews();
			this.logger.log(
				`Got news!:\n ${JSON.stringify(news)}\n\n Inserting on mongo`,
			);
			const { nInserted,nMatched, nModified, nUpserted } = await this.newService.bulkCreate(news);
			this.logger.log(`Succesfully inserted on mongo Matched: ${nMatched}, Modified: ${nModified}, Inserted: ${nInserted}, Upserted: ${nUpserted} `);
		} catch (err) {
			const { code, message } = err;
			this.logger.error(
				`An error ocurred while updating news \n code: ${code} \n message: ${message}`,
			);
		}
	}

	async getNews(): Promise<New[]> {
		const getQuery = await this.httpService.get(
			`${this.newsUrl}/search_by_date?query=nodejs`,
		);
		const response = await lastValueFrom(getQuery);
		return response.data.hits as New[];
	}
}
