import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FilterNew } from 'src/interfaces/pagination';
import { New, NewDocument } from '../models/new.model';

@Injectable()
export class NewsService {

	constructor(
		@InjectModel(New.name) private readonly newModel: Model<NewDocument>,
	) {}

	async findAll(documentFilter: FilterNew, value: any, documentLimit: number = 5): Promise<New[]> {
		return this.newModel.find({
			[documentFilter]: value
		})
		.limit(documentLimit)
		.exec();
	}

	async create(data: New): Promise<New> {
		return new this.newModel(data).save();
	}

	async bulkCreate(data: New[]) {
		const toBulks = data.map((newBulk) => {
			const bulk = {
				updateOne: {
				filter: {objectID: newBulk.objectID},
				update: newBulk,
				upsert: true,
			}}
			return bulk
		})
		return this.newModel.bulkWrite(toBulks); 
	}

	async createMany(news: New[]): Promise<New[]> {
		return this.newModel.insertMany(news);
	}
	async findOne(id: string): Promise<New> {
		return this.newModel.findOne({ _id: id });
	}
	async update(id: string, data: New): Promise<New> {
		return this.newModel.findByIdAndUpdate({ _id: id }, data);
	}
	async delete(id: string) {
		return this.newModel.deleteOne({ _id: id });
	}
}
