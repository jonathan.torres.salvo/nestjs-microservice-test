import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { setupSwagger } from './swagger.setup';
async function bootstrap() {
	const port: string = process.env.APP_PORT ? process.env.APP_PORT : '3000';
	const app = await NestFactory.create(AppModule);
	app.useGlobalPipes(new ValidationPipe())
	app.setGlobalPrefix('api');
	setupSwagger(app);
	await app
		.listen(port)
		.then(() => Logger.log(`Service listening on port: ${port}`)); // APP_PORT
}
bootstrap();
