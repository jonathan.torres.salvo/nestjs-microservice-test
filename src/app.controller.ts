import { Controller, Get, Request, Post, Body } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { Public } from './auth/constants';
import { User } from './interfaces/user';

@Controller()
export class AppController {
	constructor(private authService: AuthService, private appService: AppService) {}
	
	@Public()
	@Post('auth/login')
	async login(@Body() {username, password}: User) {
		return this.authService.login({username, password});
	}
	
}
