import { INestApplication } from "@nestjs/common";
import { DocumentBuilder, SwaggerDocumentOptions, SwaggerModule} from "@nestjs/swagger";
export function setupSwagger (app: INestApplication) {
    const config = new DocumentBuilder().setTitle('Node News API')
    .setDescription('NodeJS News API made using NestJS')
    .addTag('News')
    .addSecurity('bearer',{
        type: 'apiKey',
        scheme: 'bearer'
    }).build();
    const swaggerCfg: SwaggerDocumentOptions = {
        ignoreGlobalPrefix: false,
        deepScanRoutes: false,
    }
	const document = SwaggerModule.createDocument(app, config, swaggerCfg);
	SwaggerModule.setup("api/docs", app, document);

}