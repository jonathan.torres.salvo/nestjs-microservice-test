import { Test } from '@nestjs/testing';
import { NewsController } from '../../src/news/news.controller';
import { NewsService } from '../../src/news/news.service';
import { mockNew, mockNews } from '../mocks/news';
import { getModelToken } from '@nestjs/mongoose';
import { New } from '../../src/models/new.model';
import { mongoMocks } from '../mocks/mongoMocks';
import { PaginationParams, FilterNew } from '../../src/interfaces/pagination';

describe('News Test', () => {
    let newsController: NewsController;
    let newsService: NewsService;

    beforeEach(async () => {

        const moduleRef = await Test.createTestingModule({
            controllers: [NewsController],
            providers: [NewsService,
            {
                provide: getModelToken('New'),
                useValue: mongoMocks
            }],
        }).compile();

        newsService = moduleRef.get<NewsService>(NewsService);
        newsController = moduleRef.get<NewsController>(NewsController);
    })

    describe('Controller Tests', ()=>{
        it('Should find all news', async () => {
            jest.spyOn(newsService, 'findAll').mockImplementationOnce(() => Promise.resolve(Promise.resolve(mockNews)))
            const pagi: PaginationParams = {
                filter_by: FilterNew.author,
                value: 'samx07',
                limit: 3
                
            }
            const newsResp = await newsController.all(pagi);
            expect(newsResp).toBe(mockNews);
        })
        it('Should find one New', async () => {
            jest.spyOn(newsService, 'findOne').mockImplementationOnce(() => Promise.resolve(Promise.resolve(mockNew)))
            const newResp = await newsController.findOne('dummyobjectID1');
            expect(newResp).toBe(mockNew);
        })
    })
    describe('Service Tests', ()=>{
        it('Should find all news', async () => {
            
            const pagi: PaginationParams = {
                filter_by: FilterNew.author,
                value: 'samx07',
                limit: 3
                
            }
            const newsResp = await newsService.findAll(pagi.filter_by, pagi.value, pagi.limit);
            expect(newsResp).toBe(mockNews);
        })
        it('Should find one New', async () => {
            jest.spyOn(newsService, 'findOne').mockImplementationOnce(() => Promise.resolve(Promise.resolve(mockNew)))
            const newResp = await newsController.findOne('dummyobjectID1');
            expect(newResp).toBe(mockNew);
        })
    })
})