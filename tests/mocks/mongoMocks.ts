import { mockNews } from "./news"

export const mongoMocks = {
    find: ()=> {
        return {
            limit: ()=> {
                return { exec: () => mockNews }
            },
            exec: ()=> mockNews
        }
    }
}